﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic.FileIO;

namespace Monsterkit_Reborn
{
    public partial class Form1 : Form
    {
        MonsterCatalogue monsterCatalogue;

        public Form1()
        {
            InitializeComponent();

            cb_pl.SelectedIndex = 0;

            monsterCatalogue = new MonsterCatalogue();

            List<string> monsterNames = new List<string>();

            for(int i = 0; i < monsterCatalogue.MonsterCount(); i++)
            {             
                monsterNames.Add(monsterCatalogue.GetMonster(i).Name);
            }

            monsterNames.Sort();
            cb_ms.Items.AddRange(monsterNames.ToArray());
            cb_sc.Items.AddRange(monsterNames.ToArray());

            cb_ms.SelectedIndex = 0;
            cb_sc.Text = monsterNames[0];
        }

        List<Encounter> readEncounters(string filename="encounterdb.csv", string delimiter=";")
        {
            List<Encounter> encounters = new List<Encounter>();

            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.ReadFields();  //reading the header
                
                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();

                    string map = fields[0];
                    string coords = fields[1];
                    string m1Name = fields[2];
                    int m1Level = Int32.Parse(fields[3]);
                    string m2Name = fields[4];
                    int m2Level;

                    if (m2Name == "(Alone)")
                    {
                        m2Level = 0;
                    }
                    else
                    {
                        m2Level = Int32.Parse(fields[5]);
                    }

                    Monster monster1 = monsterCatalogue.MonsterSearch(m1Name);
                    int attack1 = monster1.CalculateAttack(m1Level);
                    int defense1 = monster1.CalculateDefense(m1Level);
                    int dexterity1 = monster1.CalculateDexterity(m1Level);
                    int life1 = monster1.CalculateLife(m1Level);
                    string element1 = monster1.Element;

                    Monster monster2 = monsterCatalogue.MonsterSearch(m2Name);
                    int attack2 = 0;
                    int defense2 = 0;
                    int dexterity2 = 0;
                    int life2 = 0;
                    string element2 = "";

                    if (monster2 != null)
                    {
                        attack2 = monster2.CalculateAttack(m2Level);
                        defense2 = monster2.CalculateDefense(m2Level);
                        dexterity2 = monster2.CalculateDexterity(m2Level);
                        life2 = monster2.CalculateLife(m2Level);
                        element2 = monster2.Element;
                    }

                    Encounter encounter = new Encounter(map, coords, m1Name, m1Level, attack1, defense1, dexterity1, life1, element1, m2Name, m2Level, attack2, defense2, dexterity2, life2, element2);
                    encounters.Add(encounter);
                }
            }

            return encounters;
        }

        private void btn_pl_Click(object sender, EventArgs e)
        {
            List<Encounter> encounters = readEncounters();
            List<Encounter> validEncounters = new List<Encounter>();
            int defense = Int32.Parse(tb_pl.Text);

            foreach (Encounter encounter in encounters)
            {
                int element = cb_pl.SelectedIndex;
                int attack1 = encounter.M1Attack * 2;
                int attack2 = encounter.M2Attack * 2;
                int element1 = Array.IndexOf(GlobalVariables.ELEMENTS, encounter.M1Element);
                int element2 = Array.IndexOf(GlobalVariables.ELEMENTS, encounter.M2Element);
                bool effective = false;

                if ((element + 1) % GlobalVariables.ELEMENTS.Length == element1)
                {
                    effective = true;
                    attack1 /= 2;
                }

                if (element2 != -1)
                {
                    if ((element + 1) % GlobalVariables.ELEMENTS.Length == element2)
                    {
                        attack2 /= 2;
                    }
                    else
                    {
                        effective = false;
                    }
                }

                if (defense >= attack1 && defense >= attack2)
                {
                    if (chk_pl.Checked)
                    {
                        if (effective)
                        {
                            validEncounters.Add(encounter);
                        }
                    }
                    else
                    {
                        validEncounters.Add(encounter);
                    }
                }
            }

            //Show the data in the grid

            dgv_encounters.Rows.Clear();

            foreach (Encounter encounter in validEncounters)
            {
                try
                {
                    dgv_encounters.Rows.Add(encounter.Map, encounter.Coords, encounter.M1Name, encounter.M1Level, encounter.M1Attack, encounter.M1Defense, encounter.M1Dexterity, encounter.M1Life, encounter.M1Element, encounter.M2Name, encounter.M2Level, encounter.M2Attack, encounter.M2Defense, encounter.M2Dexterity, encounter.M2Life, encounter.M2Element);
                }
                catch
                {
                    //ERROR
                }
            }

            dgv_encounters.Sort(dgv_encounters.Columns[3], ListSortDirection.Descending);
        }

        private void btn_tl_Click(object sender, EventArgs e)
        {
            List<Encounter> encounters = readEncounters();
            List<Encounter> validEncounters = new List<Encounter>();
            int defense = Int32.Parse(tb_tl.Text);

            foreach (Encounter encounter in encounters)
            {
                int attack1 = encounter.M1Attack * 2;
                int attack2 = encounter.M2Attack * 2;
                                
                if (defense >= attack1 && defense >= attack2)
                {
                    validEncounters.Add(encounter);
                }
            }

            //Show the data in the grid

            dgv_encounters.Rows.Clear();

            foreach (Encounter encounter in validEncounters)
            {
                try
                {
                    dgv_encounters.Rows.Add(encounter.Map, encounter.Coords, encounter.M1Name, encounter.M1Level, encounter.M1Attack, encounter.M1Defense, encounter.M1Dexterity, encounter.M1Life, encounter.M1Element, encounter.M2Name, encounter.M2Level, encounter.M2Attack, encounter.M2Defense, encounter.M2Dexterity, encounter.M2Life, encounter.M2Element);
                }
                catch
                {
                    //ERROR
                }
            }

            dgv_encounters.Sort(dgv_encounters.Columns[3], ListSortDirection.Descending);
        }

        private void btn_ms_Click(object sender, EventArgs e)
        {
            string monsterName = (string)cb_ms.SelectedItem;

            List<Encounter> encounters = readEncounters();
            List<Encounter> validEncounters = new List<Encounter>();

            foreach (Encounter encounter in encounters)
            {
                if (encounter.M1Name == monsterName || encounter.M2Name == monsterName)
                {
                    validEncounters.Add(encounter);
                }
            }

            //Show the data in the grid

            dgv_encounters.Rows.Clear();

            foreach (Encounter encounter in validEncounters)
            {
                try
                {
                    dgv_encounters.Rows.Add(encounter.Map, encounter.Coords, encounter.M1Name, encounter.M1Level, encounter.M1Attack, encounter.M1Defense, encounter.M1Dexterity, encounter.M1Life, encounter.M1Element, encounter.M2Name, encounter.M2Level, encounter.M2Attack, encounter.M2Defense, encounter.M2Dexterity, encounter.M2Life, encounter.M2Element);
                }
                catch
                {
                    //ERROR
                }
            }

            dgv_encounters.Sort(dgv_encounters.Columns[3], ListSortDirection.Descending);
        }

        private void btn_sc_Click(object sender, EventArgs e)
        {

            label12.Visible = true;
            tb_sc_attgr.Visible = true;
            label13.Visible = true;
            tb_sc_lifegr.Visible = true;
            label14.Visible = true;
            tb_sc_attackpercentage.Visible = true;
            label15.Visible = true;
            tb_sc_defensepercentage.Visible = true;
            label16.Visible = true;
            tb_sc_dexteritypercentage.Visible = true;

            string monsterName = (string)cb_sc.SelectedItem;
            Monster monster = monsterCatalogue.MonsterSearch(monsterName);

            if (Int32.Parse(tb_sc_level.Text) <= 1)
            {
                tb_sc_level.Text = "1";
            }

            int monsterLevel = Int32.Parse(tb_sc_level.Text);

            if (float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture) <= 0.0f)
            {
                tb_sc_attgr.Text = monster.AttGR.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_lifegr.Text, CultureInfo.InvariantCulture) <= 0.0f)
            {
                tb_sc_lifegr.Text = monster.LifeGR.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_attackpercentage.Text, CultureInfo.InvariantCulture) <= 0.0f || float.Parse(tb_sc_attackpercentage.Text, CultureInfo.InvariantCulture) >= 100)
            {
                tb_sc_attackpercentage.Text = monster.AttackPercentage.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_defensepercentage.Text, CultureInfo.InvariantCulture) <= 0.0f || float.Parse(tb_sc_defensepercentage.Text, CultureInfo.InvariantCulture) >= 100)
            {
                tb_sc_defensepercentage.Text = monster.DefensePercentage.ToString("0.00", CultureInfo.InvariantCulture);
            }

            if (float.Parse(tb_sc_dexteritypercentage.Text, CultureInfo.InvariantCulture) <= 0.0f || float.Parse(tb_sc_dexteritypercentage.Text, CultureInfo.InvariantCulture) >= 100)
            {
                tb_sc_dexteritypercentage.Text = monster.DexterityPercentage.ToString("0.00", CultureInfo.InvariantCulture);
            }

            int attack = monster.CalculateAttack(monsterLevel, float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture), float.Parse(tb_sc_attackpercentage.Text, CultureInfo.InvariantCulture));
            int defense = monster.CalculateDefense(monsterLevel, float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture), float.Parse(tb_sc_defensepercentage.Text, CultureInfo.InvariantCulture));
            int dexterity = monster.CalculateDexterity(monsterLevel, float.Parse(tb_sc_attgr.Text, CultureInfo.InvariantCulture), float.Parse(tb_sc_dexteritypercentage.Text, CultureInfo.InvariantCulture));
            int life = monster.CalculateLife(monsterLevel, float.Parse(tb_sc_lifegr.Text, CultureInfo.InvariantCulture));
            tb_sc_attack.Text = "" + attack;
            tb_sc_defense.Text = "" + defense;
            tb_sc_dexterity.Text = "" + dexterity;
            tb_sc_life.Text = "" + life;
        }

        private void cb_sc_SelectionChangeCommitted(object sender, EventArgs e)
        {
            label12.Visible = true;
            tb_sc_attgr.Visible = true;
            label13.Visible = true;
            tb_sc_lifegr.Visible = true;
            label14.Visible = true;
            tb_sc_attackpercentage.Visible = true;
            label15.Visible = true;
            tb_sc_defensepercentage.Visible = true;
            label16.Visible = true;
            tb_sc_dexteritypercentage.Visible = true;

            string monsterName = (string)cb_sc.SelectedItem;
            Monster monster = monsterCatalogue.MonsterSearch(monsterName);
            tb_sc_attgr.Text = monster.AttGR.ToString("0.00", CultureInfo.InvariantCulture);
            tb_sc_lifegr.Text = monster.LifeGR.ToString("0.00", CultureInfo.InvariantCulture);

            tb_sc_attackpercentage.Text = monster.AttackPercentage.ToString("0.00", CultureInfo.InvariantCulture);
            tb_sc_defensepercentage.Text = monster.DefensePercentage.ToString("0.00", CultureInfo.InvariantCulture);
            tb_sc_dexteritypercentage.Text = monster.DexterityPercentage.ToString("0.00", CultureInfo.InvariantCulture);
        }
    }
}
