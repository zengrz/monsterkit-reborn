﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monsterkit_Reborn
{
    class Encounter
    {
        string map, coords;
        string m1Name, m2Name, m1Element, m2Element;
        int m1Level, m1Attack, m1Defense, m1Dexterity, m1Life, m2Level, m2Attack, m2Defense, m2Dexterity, m2Life;

        public string Map
        {
            get { return map; }
        }

        public string Coords
        {
            get { return coords; }
            set
            {
                if (value.StartsWith("(") && value.EndsWith(")"))
                {
                    coords = value;
                }
                else
                {
                    coords = "-";
                }
            }
        }

        public string M1Name
        {
            get { return m1Name; }
        }

        public string M1Element
        {
            get { return m1Element; }
        }

        public int M1Attack
        {
            get { return m1Attack; }
        }

        public int M1Defense
        {
            get { return m1Defense; }
        }

        public int M1Dexterity
        {
            get { return m1Dexterity; }
        }

        public int M1Life
        {
            get { return m1Life; }
        }

        public int M1Level
        {
            get { return m1Level; }
        }

        public string M2Name
        {
            get { return m2Name; }
        }

        public string M2Element
        {
            get { return m2Element; }
        }

        public int M2Attack
        {
            get { return m2Attack; }
        }

        public int M2Defense
        {
            get { return m2Defense; }
        }

        public int M2Dexterity
        {
            get { return m2Dexterity; }
        }

        public int M2Life
        {
            get { return m2Life; }
        }

        public int M2Level
        {
            get { return m2Level; }
        }

        public Encounter(string map, string coords, string m1Name, int m1Level, int m1Attack, int m1Defense, int m1Dexterity, int m1Life, string m1Element, string m2Name, int m2Level, int m2Attack, int m2Defense, int m2Dexterity, int m2Life, string m2Element)
        {
            this.map = map;
            Coords = coords;
            this.m1Name = m1Name;
            this.m1Level = m1Level;
            this.m1Attack = m1Attack;
            this.m1Defense = m1Defense;
            this.m1Dexterity = m1Dexterity;
            this.m1Life = m1Life;
            this.m1Element = m1Element;
            this.m2Name = m2Name;
            this.m2Level = m2Level;
            this.m2Attack = m2Attack;
            this.m2Defense = m2Defense;
            this.m2Dexterity = m2Dexterity;
            this.m2Life = m2Life;
            this.m2Element = m2Element;
        }
    }
}
