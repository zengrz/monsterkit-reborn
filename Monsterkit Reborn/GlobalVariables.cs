﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monsterkit_Reborn
{
    public static class GlobalVariables
    {
        public const int MAX_LEVEL = 8000;
        public const int MAX_STAT = 999999;
        public static readonly string[] ELEMENTS = { "Water", "Fire", "Metal", "Wood", "Earth" }; //Water>Fire, Fire>Metal, Metal>Wood, Wood>Earth, Earth>Water
    }
}
