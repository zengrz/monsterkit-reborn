﻿namespace Monsterkit_Reborn
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.dvg_encounters_m2element = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m2life = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m2dexterity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m2defense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m2attack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m2level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m2name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1element = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1life = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1dexterity = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1defense = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1attack = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1level = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_m1name = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_coords = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dvg_encounters_map = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_encounters = new System.Windows.Forms.DataGridView();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.cb_ms = new System.Windows.Forms.ComboBox();
            this.btn_ms = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tb_tl = new System.Windows.Forms.TextBox();
            this.btn_tl = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tb_pl = new System.Windows.Forms.TextBox();
            this.cb_pl = new System.Windows.Forms.ComboBox();
            this.chk_pl = new System.Windows.Forms.CheckBox();
            this.btn_pl = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.tb_sc_dexteritypercentage = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.tb_sc_defensepercentage = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.tb_sc_attackpercentage = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.tb_sc_lifegr = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.tb_sc_attgr = new System.Windows.Forms.TextBox();
            this.tb_sc_life = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tb_sc_dexterity = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tb_sc_defense = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tb_sc_attack = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.tb_sc_level = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.cb_sc = new System.Windows.Forms.ComboBox();
            this.btn_sc = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_encounters)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.SuspendLayout();
            // 
            // dvg_encounters_m2element
            // 
            this.dvg_encounters_m2element.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2element.HeaderText = "Element";
            this.dvg_encounters_m2element.Name = "dvg_encounters_m2element";
            this.dvg_encounters_m2element.ReadOnly = true;
            this.dvg_encounters_m2element.Width = 70;
            // 
            // dvg_encounters_m2life
            // 
            this.dvg_encounters_m2life.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2life.HeaderText = "Life";
            this.dvg_encounters_m2life.Name = "dvg_encounters_m2life";
            this.dvg_encounters_m2life.ReadOnly = true;
            this.dvg_encounters_m2life.Width = 49;
            // 
            // dvg_encounters_m2dexterity
            // 
            this.dvg_encounters_m2dexterity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2dexterity.HeaderText = "Dexterity";
            this.dvg_encounters_m2dexterity.Name = "dvg_encounters_m2dexterity";
            this.dvg_encounters_m2dexterity.ReadOnly = true;
            this.dvg_encounters_m2dexterity.Width = 73;
            // 
            // dvg_encounters_m2defense
            // 
            this.dvg_encounters_m2defense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2defense.HeaderText = "Defense";
            this.dvg_encounters_m2defense.Name = "dvg_encounters_m2defense";
            this.dvg_encounters_m2defense.ReadOnly = true;
            this.dvg_encounters_m2defense.Width = 72;
            // 
            // dvg_encounters_m2attack
            // 
            this.dvg_encounters_m2attack.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2attack.HeaderText = "Attack";
            this.dvg_encounters_m2attack.Name = "dvg_encounters_m2attack";
            this.dvg_encounters_m2attack.ReadOnly = true;
            this.dvg_encounters_m2attack.Width = 63;
            // 
            // dvg_encounters_m2level
            // 
            this.dvg_encounters_m2level.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2level.HeaderText = "Level";
            this.dvg_encounters_m2level.Name = "dvg_encounters_m2level";
            this.dvg_encounters_m2level.ReadOnly = true;
            this.dvg_encounters_m2level.Width = 58;
            // 
            // dvg_encounters_m2name
            // 
            this.dvg_encounters_m2name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m2name.HeaderText = "Name";
            this.dvg_encounters_m2name.Name = "dvg_encounters_m2name";
            this.dvg_encounters_m2name.ReadOnly = true;
            this.dvg_encounters_m2name.Width = 60;
            // 
            // dvg_encounters_m1element
            // 
            this.dvg_encounters_m1element.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1element.HeaderText = "Element";
            this.dvg_encounters_m1element.Name = "dvg_encounters_m1element";
            this.dvg_encounters_m1element.ReadOnly = true;
            this.dvg_encounters_m1element.Width = 70;
            // 
            // dvg_encounters_m1life
            // 
            this.dvg_encounters_m1life.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1life.HeaderText = "Life";
            this.dvg_encounters_m1life.Name = "dvg_encounters_m1life";
            this.dvg_encounters_m1life.ReadOnly = true;
            this.dvg_encounters_m1life.Width = 49;
            // 
            // dvg_encounters_m1dexterity
            // 
            this.dvg_encounters_m1dexterity.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1dexterity.HeaderText = "Dexterity";
            this.dvg_encounters_m1dexterity.Name = "dvg_encounters_m1dexterity";
            this.dvg_encounters_m1dexterity.ReadOnly = true;
            this.dvg_encounters_m1dexterity.Width = 73;
            // 
            // dvg_encounters_m1defense
            // 
            this.dvg_encounters_m1defense.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1defense.HeaderText = "Defense";
            this.dvg_encounters_m1defense.Name = "dvg_encounters_m1defense";
            this.dvg_encounters_m1defense.ReadOnly = true;
            this.dvg_encounters_m1defense.Width = 72;
            // 
            // dvg_encounters_m1attack
            // 
            this.dvg_encounters_m1attack.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1attack.HeaderText = "Attack";
            this.dvg_encounters_m1attack.Name = "dvg_encounters_m1attack";
            this.dvg_encounters_m1attack.ReadOnly = true;
            this.dvg_encounters_m1attack.Width = 63;
            // 
            // dvg_encounters_m1level
            // 
            this.dvg_encounters_m1level.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1level.HeaderText = "Level";
            this.dvg_encounters_m1level.Name = "dvg_encounters_m1level";
            this.dvg_encounters_m1level.ReadOnly = true;
            this.dvg_encounters_m1level.Width = 58;
            // 
            // dvg_encounters_m1name
            // 
            this.dvg_encounters_m1name.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_m1name.HeaderText = "Name";
            this.dvg_encounters_m1name.Name = "dvg_encounters_m1name";
            this.dvg_encounters_m1name.ReadOnly = true;
            this.dvg_encounters_m1name.Width = 60;
            // 
            // dvg_encounters_coords
            // 
            this.dvg_encounters_coords.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_coords.HeaderText = "Coords";
            this.dvg_encounters_coords.Name = "dvg_encounters_coords";
            this.dvg_encounters_coords.ReadOnly = true;
            this.dvg_encounters_coords.Width = 65;
            // 
            // dvg_encounters_map
            // 
            this.dvg_encounters_map.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.dvg_encounters_map.HeaderText = "Map";
            this.dvg_encounters_map.Name = "dvg_encounters_map";
            this.dvg_encounters_map.ReadOnly = true;
            this.dvg_encounters_map.Width = 53;
            // 
            // dgv_encounters
            // 
            this.dgv_encounters.AllowUserToAddRows = false;
            this.dgv_encounters.AllowUserToDeleteRows = false;
            this.dgv_encounters.AllowUserToOrderColumns = true;
            this.dgv_encounters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_encounters.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dvg_encounters_map,
            this.dvg_encounters_coords,
            this.dvg_encounters_m1name,
            this.dvg_encounters_m1level,
            this.dvg_encounters_m1attack,
            this.dvg_encounters_m1defense,
            this.dvg_encounters_m1dexterity,
            this.dvg_encounters_m1life,
            this.dvg_encounters_m1element,
            this.dvg_encounters_m2name,
            this.dvg_encounters_m2level,
            this.dvg_encounters_m2attack,
            this.dvg_encounters_m2defense,
            this.dvg_encounters_m2dexterity,
            this.dvg_encounters_m2life,
            this.dvg_encounters_m2element});
            this.dgv_encounters.Location = new System.Drawing.Point(4, 163);
            this.dgv_encounters.Name = "dgv_encounters";
            this.dgv_encounters.ReadOnly = true;
            this.dgv_encounters.RowHeadersVisible = false;
            this.dgv_encounters.Size = new System.Drawing.Size(716, 171);
            this.dgv_encounters.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ImageAlign = System.Drawing.ContentAlignment.TopRight;
            this.label1.Location = new System.Drawing.Point(551, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 39);
            this.label1.TabIndex = 2;
            this.label1.Text = "Created by: luckymouse0\r\n\r\nInspired in Monsterkit by GiulianuB";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.cb_ms);
            this.tabPage3.Controls.Add(this.btn_ms);
            this.tabPage3.Controls.Add(this.label5);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(385, 137);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Monster Search";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // cb_ms
            // 
            this.cb_ms.FormattingEnabled = true;
            this.cb_ms.Location = new System.Drawing.Point(88, 36);
            this.cb_ms.Name = "cb_ms";
            this.cb_ms.Size = new System.Drawing.Size(90, 21);
            this.cb_ms.TabIndex = 11;
            // 
            // btn_ms
            // 
            this.btn_ms.Location = new System.Drawing.Point(45, 93);
            this.btn_ms.Name = "btn_ms";
            this.btn_ms.Size = new System.Drawing.Size(75, 23);
            this.btn_ms.TabIndex = 10;
            this.btn_ms.Text = "Search";
            this.btn_ms.UseVisualStyleBackColor = true;
            this.btn_ms.Click += new System.EventHandler(this.btn_ms_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 39);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Monster Name:";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tb_tl);
            this.tabPage2.Controls.Add(this.btn_tl);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(385, 137);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Player Leveling";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tb_tl
            // 
            this.tb_tl.Location = new System.Drawing.Point(91, 36);
            this.tb_tl.MaxLength = 9;
            this.tb_tl.Name = "tb_tl";
            this.tb_tl.Size = new System.Drawing.Size(70, 20);
            this.tb_tl.TabIndex = 8;
            this.tb_tl.Text = "0";
            // 
            // btn_tl
            // 
            this.btn_tl.Location = new System.Drawing.Point(45, 93);
            this.btn_tl.Name = "btn_tl";
            this.btn_tl.Size = new System.Drawing.Size(75, 23);
            this.btn_tl.TabIndex = 7;
            this.btn_tl.Text = "Search";
            this.btn_tl.UseVisualStyleBackColor = true;
            this.btn_tl.Click += new System.EventHandler(this.btn_tl_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 39);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Player Defense:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tb_pl);
            this.tabPage1.Controls.Add(this.cb_pl);
            this.tabPage1.Controls.Add(this.chk_pl);
            this.tabPage1.Controls.Add(this.btn_pl);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(385, 137);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Pet Leveling";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tb_pl
            // 
            this.tb_pl.Location = new System.Drawing.Point(78, 13);
            this.tb_pl.MaxLength = 9;
            this.tb_pl.Name = "tb_pl";
            this.tb_pl.Size = new System.Drawing.Size(70, 20);
            this.tb_pl.TabIndex = 5;
            this.tb_pl.Text = "0";
            // 
            // cb_pl
            // 
            this.cb_pl.FormattingEnabled = true;
            this.cb_pl.Items.AddRange(new object[] {
            "Water",
            "Fire",
            "Metal",
            "Wood",
            "Earth"});
            this.cb_pl.Location = new System.Drawing.Point(78, 36);
            this.cb_pl.Name = "cb_pl";
            this.cb_pl.Size = new System.Drawing.Size(70, 21);
            this.cb_pl.TabIndex = 4;
            // 
            // chk_pl
            // 
            this.chk_pl.AutoSize = true;
            this.chk_pl.Location = new System.Drawing.Point(11, 66);
            this.chk_pl.Name = "chk_pl";
            this.chk_pl.Size = new System.Drawing.Size(187, 17);
            this.chk_pl.TabIndex = 3;
            this.chk_pl.Text = "Search only for elemental enemies";
            this.chk_pl.UseVisualStyleBackColor = true;
            // 
            // btn_pl
            // 
            this.btn_pl.Location = new System.Drawing.Point(45, 93);
            this.btn_pl.Name = "btn_pl";
            this.btn_pl.Size = new System.Drawing.Size(75, 23);
            this.btn_pl.TabIndex = 2;
            this.btn_pl.Text = "Search";
            this.btn_pl.UseVisualStyleBackColor = true;
            this.btn_pl.Click += new System.EventHandler(this.btn_pl_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(8, 39);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 13);
            this.label3.TabIndex = 1;
            this.label3.Text = "Pet Element:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pet Defense:";
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(0, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(393, 163);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.tb_sc_dexteritypercentage);
            this.tabPage4.Controls.Add(this.label16);
            this.tabPage4.Controls.Add(this.tb_sc_defensepercentage);
            this.tabPage4.Controls.Add(this.label15);
            this.tabPage4.Controls.Add(this.tb_sc_attackpercentage);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Controls.Add(this.tb_sc_lifegr);
            this.tabPage4.Controls.Add(this.label13);
            this.tabPage4.Controls.Add(this.tb_sc_attgr);
            this.tabPage4.Controls.Add(this.tb_sc_life);
            this.tabPage4.Controls.Add(this.label12);
            this.tabPage4.Controls.Add(this.label11);
            this.tabPage4.Controls.Add(this.tb_sc_dexterity);
            this.tabPage4.Controls.Add(this.label10);
            this.tabPage4.Controls.Add(this.tb_sc_defense);
            this.tabPage4.Controls.Add(this.label9);
            this.tabPage4.Controls.Add(this.tb_sc_attack);
            this.tabPage4.Controls.Add(this.label8);
            this.tabPage4.Controls.Add(this.tb_sc_level);
            this.tabPage4.Controls.Add(this.label7);
            this.tabPage4.Controls.Add(this.cb_sc);
            this.tabPage4.Controls.Add(this.btn_sc);
            this.tabPage4.Controls.Add(this.label6);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(385, 137);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Stats Calculator";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // tb_sc_dexteritypercentage
            // 
            this.tb_sc_dexteritypercentage.Location = new System.Drawing.Point(346, 110);
            this.tb_sc_dexteritypercentage.MaxLength = 5;
            this.tb_sc_dexteritypercentage.Name = "tb_sc_dexteritypercentage";
            this.tb_sc_dexteritypercentage.Size = new System.Drawing.Size(34, 20);
            this.tb_sc_dexteritypercentage.TabIndex = 32;
            this.tb_sc_dexteritypercentage.Text = "0";
            this.tb_sc_dexteritypercentage.Visible = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(306, 113);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(40, 13);
            this.label16.TabIndex = 31;
            this.label16.Text = "DEX%:";
            this.label16.Visible = false;
            // 
            // tb_sc_defensepercentage
            // 
            this.tb_sc_defensepercentage.Location = new System.Drawing.Point(272, 110);
            this.tb_sc_defensepercentage.MaxLength = 5;
            this.tb_sc_defensepercentage.Name = "tb_sc_defensepercentage";
            this.tb_sc_defensepercentage.Size = new System.Drawing.Size(34, 20);
            this.tb_sc_defensepercentage.TabIndex = 30;
            this.tb_sc_defensepercentage.Text = "0";
            this.tb_sc_defensepercentage.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(233, 113);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(39, 13);
            this.label15.TabIndex = 29;
            this.label15.Text = "DEF%:";
            this.label15.Visible = false;
            // 
            // tb_sc_attackpercentage
            // 
            this.tb_sc_attackpercentage.Location = new System.Drawing.Point(199, 110);
            this.tb_sc_attackpercentage.MaxLength = 5;
            this.tb_sc_attackpercentage.Name = "tb_sc_attackpercentage";
            this.tb_sc_attackpercentage.Size = new System.Drawing.Size(34, 20);
            this.tb_sc_attackpercentage.TabIndex = 28;
            this.tb_sc_attackpercentage.Text = "0";
            this.tb_sc_attackpercentage.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(160, 113);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(39, 13);
            this.label14.TabIndex = 27;
            this.label14.Text = "ATK%:";
            this.label14.Visible = false;
            // 
            // tb_sc_lifegr
            // 
            this.tb_sc_lifegr.Location = new System.Drawing.Point(144, 66);
            this.tb_sc_lifegr.MaxLength = 9;
            this.tb_sc_lifegr.Name = "tb_sc_lifegr";
            this.tb_sc_lifegr.Size = new System.Drawing.Size(36, 20);
            this.tb_sc_lifegr.TabIndex = 26;
            this.tb_sc_lifegr.Text = "0.0";
            this.tb_sc_lifegr.Visible = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(97, 69);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(46, 13);
            this.label13.TabIndex = 25;
            this.label13.Text = "Life GR:";
            this.label13.Visible = false;
            // 
            // tb_sc_attgr
            // 
            this.tb_sc_attgr.Location = new System.Drawing.Point(59, 66);
            this.tb_sc_attgr.MaxLength = 9;
            this.tb_sc_attgr.Name = "tb_sc_attgr";
            this.tb_sc_attgr.Size = new System.Drawing.Size(36, 20);
            this.tb_sc_attgr.TabIndex = 18;
            this.tb_sc_attgr.Text = "0.0";
            this.tb_sc_attgr.Visible = false;
            // 
            // tb_sc_life
            // 
            this.tb_sc_life.Location = new System.Drawing.Point(267, 82);
            this.tb_sc_life.MaxLength = 9;
            this.tb_sc_life.Name = "tb_sc_life";
            this.tb_sc_life.ReadOnly = true;
            this.tb_sc_life.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_life.TabIndex = 24;
            this.tb_sc_life.Text = "0";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(8, 69);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(50, 13);
            this.label12.TabIndex = 17;
            this.label12.Text = "3-att GR:";
            this.label12.Visible = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(234, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(27, 13);
            this.label11.TabIndex = 23;
            this.label11.Text = "Life:";
            // 
            // tb_sc_dexterity
            // 
            this.tb_sc_dexterity.Location = new System.Drawing.Point(267, 59);
            this.tb_sc_dexterity.MaxLength = 9;
            this.tb_sc_dexterity.Name = "tb_sc_dexterity";
            this.tb_sc_dexterity.ReadOnly = true;
            this.tb_sc_dexterity.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_dexterity.TabIndex = 22;
            this.tb_sc_dexterity.Text = "0";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(211, 62);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(51, 13);
            this.label10.TabIndex = 21;
            this.label10.Text = "Dexterity:";
            // 
            // tb_sc_defense
            // 
            this.tb_sc_defense.Location = new System.Drawing.Point(267, 36);
            this.tb_sc_defense.MaxLength = 9;
            this.tb_sc_defense.Name = "tb_sc_defense";
            this.tb_sc_defense.ReadOnly = true;
            this.tb_sc_defense.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_defense.TabIndex = 20;
            this.tb_sc_defense.Text = "0";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(211, 39);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(50, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Defense:";
            // 
            // tb_sc_attack
            // 
            this.tb_sc_attack.Location = new System.Drawing.Point(267, 13);
            this.tb_sc_attack.MaxLength = 9;
            this.tb_sc_attack.Name = "tb_sc_attack";
            this.tb_sc_attack.ReadOnly = true;
            this.tb_sc_attack.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_attack.TabIndex = 18;
            this.tb_sc_attack.Text = "0";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(220, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(41, 13);
            this.label8.TabIndex = 17;
            this.label8.Text = "Attack:";
            // 
            // tb_sc_level
            // 
            this.tb_sc_level.Location = new System.Drawing.Point(86, 40);
            this.tb_sc_level.MaxLength = 9;
            this.tb_sc_level.Name = "tb_sc_level";
            this.tb_sc_level.Size = new System.Drawing.Size(70, 20);
            this.tb_sc_level.TabIndex = 16;
            this.tb_sc_level.Text = "1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(8, 43);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 13);
            this.label7.TabIndex = 15;
            this.label7.Text = "Monster Level:";
            // 
            // cb_sc
            // 
            this.cb_sc.FormattingEnabled = true;
            this.cb_sc.Location = new System.Drawing.Point(86, 13);
            this.cb_sc.Name = "cb_sc";
            this.cb_sc.Size = new System.Drawing.Size(90, 21);
            this.cb_sc.TabIndex = 14;
            this.cb_sc.SelectionChangeCommitted += new System.EventHandler(this.cb_sc_SelectionChangeCommitted);
            // 
            // btn_sc
            // 
            this.btn_sc.Location = new System.Drawing.Point(45, 93);
            this.btn_sc.Name = "btn_sc";
            this.btn_sc.Size = new System.Drawing.Size(75, 23);
            this.btn_sc.TabIndex = 13;
            this.btn_sc.Text = "Calculate";
            this.btn_sc.UseVisualStyleBackColor = true;
            this.btn_sc.Click += new System.EventHandler(this.btn_sc_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(8, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(79, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Monster Name:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(740, 352);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dgv_encounters);
            this.Controls.Add(this.tabControl1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Monster&Me Monster Kit Reborn";
            ((System.ComponentModel.ISupportInitialize)(this.dgv_encounters)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2element;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2life;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2dexterity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2defense;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2attack;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2level;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m2name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1element;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1life;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1dexterity;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1defense;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1attack;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1level;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_m1name;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_coords;
        private System.Windows.Forms.DataGridViewTextBoxColumn dvg_encounters_map;
        private System.Windows.Forms.DataGridView dgv_encounters;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.ComboBox cb_ms;
        private System.Windows.Forms.Button btn_ms;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox tb_tl;
        private System.Windows.Forms.Button btn_tl;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TextBox tb_pl;
        private System.Windows.Forms.ComboBox cb_pl;
        private System.Windows.Forms.CheckBox chk_pl;
        private System.Windows.Forms.Button btn_pl;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.TextBox tb_sc_dexteritypercentage;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox tb_sc_defensepercentage;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox tb_sc_attackpercentage;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox tb_sc_lifegr;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox tb_sc_attgr;
        private System.Windows.Forms.TextBox tb_sc_life;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox tb_sc_dexterity;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox tb_sc_defense;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox tb_sc_attack;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tb_sc_level;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cb_sc;
        private System.Windows.Forms.Button btn_sc;
        private System.Windows.Forms.Label label6;
    }
}

