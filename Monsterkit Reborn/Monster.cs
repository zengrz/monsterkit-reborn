﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monsterkit_Reborn
{
    class Monster
    {
        string name, element;
        int attackPercentage, defensePercentage, dexterityPercentage;
        float baseAttack, baseDefense, baseDexterity, baseLife, attGR, lifeGR;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string Element
        {
            get { return element; }
            set
            {
                if (GlobalVariables.ELEMENTS.Contains(value))
                {
                    element = value;
                }
                else
                {
                    element = "";
                }
            }
        }

        public float BaseAttack
        {
            get { return baseAttack; }
            set
            {
                if (value >= 1 && value <= GlobalVariables.MAX_STAT)
                {
                    baseAttack = value;
                }
                else
                {
                    baseAttack = 0;
                }
            }
        }

        public float BaseDefense
        {
            get { return baseDefense; }
            set
            {
                if (value >= 1 && value <= GlobalVariables.MAX_STAT)
                {
                    baseDefense = value;
                }
                else
                {
                    baseDefense = 0;
                }
            }
        }

        public float BaseDexterity
        {
            get { return baseDexterity; }
            set
            {
                if (value >= 1 && value <= GlobalVariables.MAX_STAT)
                {
                    baseDexterity = value;
                }
                else
                {
                    baseDexterity = 0;
                }
            }
        }

        public float BaseLife
        {
            get { return baseLife; }
            set
            {
                if (value >= 1 && value <= GlobalVariables.MAX_STAT)
                {
                    baseLife = value;
                }
                else
                {
                    baseLife = 0;
                }
            }
        }

        public float AttGR
        {
            get { return attGR; }
            set
            {
                if (value >= 0 && value <= GlobalVariables.MAX_STAT)
                {
                    attGR = value;
                }
                else
                {
                    attGR = 0;
                }
            }
        }

        public float LifeGR
        {
            get { return lifeGR; }
            set
            {
                if (value >= 0 && value <= GlobalVariables.MAX_STAT)
                {
                    lifeGR = value;
                }
                else
                {
                    lifeGR = 0;
                }
            }
        }
        
        public int AttackPercentage
        {
            get { return attackPercentage; }
            set
            {
                if (value < 0)
                {
                    attackPercentage = 0;
                }                    
                else
                {
                    if (value > 100)
                    {
                        attackPercentage = 100;
                    }
                    else
                    {
                        attackPercentage = value;
                    }                        
                }
            }
        }

        public int DefensePercentage
        {
            get { return defensePercentage; }
            set
            {
                if (value < 0)
                {
                    defensePercentage = 0;
                }
                else
                {
                    if (value > 100)
                    {
                        defensePercentage = 100;
                    }
                    else
                    {
                        defensePercentage = value;
                    }
                }
            }
        }

        public int DexterityPercentage
        {
            get { return dexterityPercentage; }
            set
            {
                if (value < 0)
                {
                    dexterityPercentage = 0;
                }
                else
                {
                    if (value > 100)
                    {
                        dexterityPercentage = 100;
                    }
                    else
                    {
                        dexterityPercentage = value;
                    }
                }
            }
        }

        public Monster(string name, string element, float attack, float defense, float dexterity, float life, float attGR, float lifeGR, int attackPercentage, int defensePercentage, int dexterityPercentage)
        {
            Name = name;
            Element = element;
            BaseAttack = attack;
            BaseDefense = defense;
            BaseDexterity = dexterity;
            BaseLife = life;
            AttGR = attGR;
            LifeGR = lifeGR;
            AttackPercentage = attackPercentage;
            DefensePercentage = defensePercentage;
            DexterityPercentage = dexterityPercentage;
        }

        public int CalculateAttack(int level)
        {
            return (int)Math.Floor((baseAttack + (level-1) * (attGR * 0.84) * ((float)(attackPercentage) / 100)));
        }

        public int CalculateDefense(int level)
        {
            return (int)Math.Floor((baseDefense + (level - 1) * (attGR * 0.84) * ((float)(defensePercentage) / 100)));
        }        

        public int CalculateDexterity(int level)
        {
            return (int)Math.Floor((baseDexterity + (level - 1) * (attGR * 0.84) * ((float)(dexterityPercentage) / 100)));
        }       

        public int CalculateLife(int level)
        {
            return (int)Math.Floor(baseLife + (level - 1) * (lifeGR * 0.84));
        }

        public int CalculateAttack(int level, float attGR, float attackPercentage)
        {
            return (int)Math.Floor((baseAttack + (level - 1) * attGR * (attackPercentage / 100)));
        }

        public int CalculateDefense(int level, float attGR, float defensePercentage)
        {
            return (int)Math.Floor((baseDefense + (level - 1) * attGR * (defensePercentage / 100)));
        }

        public int CalculateDexterity(int level, float attGR, float dexterityPercentage)
        {
            return (int)Math.Floor((baseDexterity + (level - 1) * attGR * (dexterityPercentage / 100)));
        }

        public int CalculateLife(int level, float lifeGR)
        {
            return (int)Math.Floor(baseLife + (level - 1) * lifeGR);
        }
    }
}
