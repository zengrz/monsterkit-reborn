﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualBasic.FileIO;

namespace Monsterkit_Reborn
{
    class MonsterCatalogue
    {
        List<Monster> catalogue;

        public MonsterCatalogue()
        {
            catalogue = new List<Monster>();
            LoadCatalogue();
        }

        public void LoadCatalogue(string filename="monsterdb.csv", string delimiter=",")
        {
            using (TextFieldParser parser = new TextFieldParser(filename))
            {
                parser.TextFieldType = FieldType.Delimited;
                parser.SetDelimiters(delimiter);
                parser.ReadFields();

                while (!parser.EndOfData)
                {
                    //Processing row
                    string[] fields = parser.ReadFields();

                    //Name,Element,Attack,Defense,Dexterity,Life,3-att GR,Life GR,Attack%,Defense%,Dexterity%
                    monsterAdd(fields[0], fields[1], float.Parse(fields[2], CultureInfo.InvariantCulture), float.Parse(fields[3], CultureInfo.InvariantCulture), float.Parse(fields[4], CultureInfo.InvariantCulture), float.Parse(fields[5], CultureInfo.InvariantCulture), float.Parse(fields[6], CultureInfo.InvariantCulture), float.Parse(fields[7], CultureInfo.InvariantCulture), Int32.Parse(fields[8]), Int32.Parse(fields[9]), Int32.Parse(fields[10]));
                }
            }
        }

        void monsterAdd(string name, string element, float attack, float defense, float dexterity, float life, float attGR, float lifeGR, int attackPercentage, int defensePercentage, int dexterityPercentage)
        {
            catalogue.Add(new Monster(name, element, attack, defense, dexterity, life, attGR, lifeGR, attackPercentage, defensePercentage, dexterityPercentage));
        }

        public Monster MonsterSearch(string name)
        {
            foreach(Monster monster in catalogue)
            {
                if(monster.Name == name)
                {
                    return monster;
                }
            }

            return null;
        }

        public Monster GetMonster(int index)
        {
            return catalogue[index];
        }

        public int MonsterCount()
        {
            return catalogue.Count;
        }
    }
}
